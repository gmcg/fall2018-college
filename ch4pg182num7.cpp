
//
// ch4pg182num7.cpp
//
// Solution for Chapter 4, Page 182, #7
// In Absolute C++, 6th Edition
// 
// Written by Gregory McGuire
//
// https://gregory.red/
// https://twitter.com/gm_mcg
// https://gitlab.com/gmcg
//

// begin includes
#include <iostream> // input/output
// #include <iomanip> // output manipulation // we're not printing any double precision values so we don't actually need this

using namespace std; // no more std::[etc] my dudes

// Important note.
// In all future projects, format all "double" answers to 2 numbers to the right of the decimal point.

// begin function prototypes
// input (one)
// output (one)
// calculation (one or more)
// some of these might be using the same calculations, but with slightly different results.
// i could probably bring back my switch statement function from chapter 3... // and i did
int ourSwitchStatement(); // reuse some code from ch3pg141num5.cpp again
void getMeasurementsFromUser(); // input, returns nothing, values are stored in variables provided by main()
void postResults(int resultKG, int resultG); // output what we have calculated
int calcKG(int userLB, int userOZ);
int calcGr(int userLB, int userOZ);

/*

		The Problem In Question...
	Write a program that will read a weight in pounds and ounces and will output
	the equivalent weight in kilograms and grams. Use at least three functions: one
	for input, one or more for calculating, and one for output. Include a loop that lets
	the user repeat this computation for new input values until the user says he or she
	wants to end the program. There are 2.2046 pounds in a kilogram, 1000 grams in
	a kilogram, and 16 ounces in a pound.

*/

// constants
// some of these might be unneeded but they're there if we want to use them
const double POUNDS_IN_1_KG = 2.2046; // the amount of pounds in a kilogram
const int GRAMS_IN_1_KG = 1000; // the amount of grams in a kilogram
const int OUNCES_IN_1_POUND = 16; // the amount of ounces in a pound

// some global variables so our input function and main can access them easily
// i'm totally not just lazy, i just don't want to have more than one (or two) function[s] for input
// since we're asking for pounds and ounces separately, we should treat them like integers.
// is it even possible to return two values from the same function?
	int userPoundsGM, // how much the user weighs, in pounds.
	    userOuncesGM; // the rest of the user's weight, in ounces.

// begin main
int main()
{

	// main variables
	int resultKGGM, resultGRGM, progTrackGM;

	// inform the user what this program does, don't really want to use a function for this one as it is only displayed once at the start of the program
	cout << "Given your how much you weigh in Pounds and Ounces, this program will calculate\nhow much you weigh in Kilograms and grams." << endl;

	for (;;) { // infinite loop, per the problem's request

		// yo what if we used an array to store the user's data
		// nah

		// base functionality
		getMeasurementsFromUser();
		resultKGGM = calcKG(userPoundsGM, userOuncesGM);
		resultGRGM = calcGr(userPoundsGM, userOuncesGM);
		cout << endl;
		postResults(resultKGGM, resultGRGM);

		for (;;) { // infinite

			// prompt the user if he/she would like to use a new set of data
			progTrackGM = ourSwitchStatement();

			if (progTrackGM == 1) {

				break; // break out of THIS for loop and continue the outer loop

			}

		}

	}

	// spacing
	cout << endl << endl;
    return 0;

}

// begin functions

// bringing back my trusty switch statement function again because honestly i don't think i can do this any differently
int ourSwitchStatement() { // a big copypaste from my chapter 3 assignments

	char willYouContinueGM; // for the switch statement

	cout << "Would you like to enter a new set of data? (Y/N): "; // god i can't really change this up
	cin >> willYouContinueGM;


	switch (willYouContinueGM) {

	case 'y':
	case 'Y':
		return 1;
		break;
	case 'n':
	case 'N':
		exit(0); // bye
		break;
	default:
		cout << "Invalid input!" << endl;
		return 0;
		break;
	}

}

void getMeasurementsFromUser() { // actually i have an idea // it didn't work

	cout << endl << "Please enter your weight in Pounds and Ounces (Whole numbers, separate with a space.): ";
	cin >> userPoundsGM >> userOuncesGM;
	cout << endl << endl;

}

int calcKG(int userLB, int userOZ) {

	// this should be simple to do
	int calculatedKG;
	double totalLB, workingOZ = userOZ, getOZ;
	
	getOZ = workingOZ / OUNCES_IN_1_POUND;
	totalLB = userLB + getOZ; // user's pounds and ounces = total weight, just to keep things accurate. we'll do this again later
	calculatedKG = totalLB / POUNDS_IN_1_KG; // grams are truncated from the result

	return calculatedKG; // pass the result back to main

}

int calcGr(int userLB, int userOZ) {

	int calculatedGrams, truncKG;
	double totalLB, workingOZ = userOZ, getOZ, calculatedKG;

	getOZ = workingOZ / OUNCES_IN_1_POUND;
	totalLB = userLB + getOZ;
	calculatedKG = totalLB / POUNDS_IN_1_KG;
	truncKG = trunc(calculatedKG); // VS2017 likes to suggest the trunc function so I decided to look it up, decided to use it here
	
	// debug
	//cout << endl << calculatedKG << " " << getOZ << endl;
	
	calculatedGrams = (calculatedKG - truncKG) * 1000;

	// I keep getting the wrong result for grams and I don't actually know why.
	// fixed by assigning userOZ to a double variable

	return calculatedGrams;

}

void postResults(int resultKG, int resultG) {

	cout << "You weigh " << resultKG << " Kilograms and " << resultG << " grams." << endl;

}

