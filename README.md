Programs I wrote for homework during my Fall 2018 semester at Quincy College.
Course: Advanced C++

This will NOT be maintained, the code is provided as-is. These programs also vary in quality and may have been done better.
Given that this is college work my professor WILL be notified of this repository.

Information on what each program does is included in each source file, and also in the book (of which you probably already have).

The MIT License applies, however I additionally ask that you do not submit this code as your own homework if you're taking a similar course with the same book.
This isn't a good practice, and you're simply hurting your own academic career in doing so.

Book: Absolute C++, 6th Edition