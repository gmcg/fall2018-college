
// schoolYear.cpp

#include <iostream>
#include <string>

using namespace std;


class schoolYear {

	public:
		schoolYear(int monthVal, int dayVal, int yearVal);
		schoolYear();
		void setStartDate();
		void showDate();
	private:
		int month;
		int day;
		int year;
		void testDate(); // test the date to see if it is valid

};

int main()
{

	schoolYear thisYear, thatYear(9, 1, 2019);

	// show the initial start date values to the user
	/*cout << "!! Printing Initial Values !!\n\n";
	thisYear.showDate();
	cout << endl << endl << endl;*/

	cout << "Making changes to thisYear...\n\n";
	thisYear.setStartDate();
	thisYear.showDate();

	cout << "\nShowing contents of thatYear...\n\n";
	thatYear.showDate();

	cout << endl; // spacing
    return 0;

}

schoolYear::schoolYear(int monthVal, int dayVal, int yearVal) {

	// in hindsight applying the changes BEFORE validating the date is a bad idea and i really shouldn't do it
	// but i'm doing it anyways
	month = monthVal;
	day = dayVal;
	year = yearVal;

	// verify
	testDate();

}

// set defaults
schoolYear::schoolYear() {

	// really wish I could just pull the system's time for this
	month = 1; 
	day = 1; 
	year = 1970; 

}

void schoolYear::setStartDate() {

	// data given to us by the user is stored in these.
	int userMonth, userDay, userYear;

	cout << "You are now setting your school's starting day!\n";
	cout << "Please enter a Month, Day, and Year (separated by a space, EX: 11 19 2018): ";
	cin >> userMonth >> userDay >> userYear;

	// store the values in the private data members
	month = userMonth;
	day = userDay;
	year = userYear;

	// verify
	testDate();

	cout << endl << endl;

}

void schoolYear::testDate() {


	int leapYearCheck = 0; // 1 if it has been determined to be a leap year

	if (year % 4 == 0) {
	
		leapYearCheck = 1;

	}


	if (month > 12 || month < 1) {
	
		cout << "Invalid Month Value! (" << month << ")\n(Is it less than 1 or greater than 12?)\n";
		exit(-1);

	}

	if (day > 31 || day < 1) {

		cout << "Invalid Day Value! (" << day << ")\n(Is it a value outside of the range of 1-31?)\n";
		exit(-1);

	}

	if (day > 29 && month == 2 && leapYearCheck == 1) {
	
		cout << "Invalid Day Value! (" << day << ")\n(Have you gone over 29 when the month was 2, and the year being a leap year?)\n";
		exit(-1);

	}

	if (day > 28 && month == 2 && leapYearCheck == 0) {
	
		cout << "Invalid Day Value! (" << day << ")\n(Have you gone over 28 when the month was 2, and the year not being a leap year?)\n";
		exit(-1);

	}

	if (year < 0) {
	
		cout << "Invalid Year Value! (" << year << ")\n(Is it negative?)\n";
		exit(-1);

	}

}

void schoolYear::showDate() {

	int theMonth = month;
	string monthName;

	switch (theMonth) {
	
		case 1:
			monthName = "January";
			break;
		case 2:
			monthName = "February";
			break;
		case 3:
			monthName = "March";
			break;
		case 4:
			monthName = "April";
			break;
		case 5:
			monthName = "May";
			break;
		case 6:
			monthName = "June";
			break;
		case 7:
			monthName = "July";
			break;
		case 8:
			monthName = "August";
			break;
		case 9:
			monthName = "September";
			break;
		case 10:
			monthName = "October";
			break;
		case 11:
			monthName = "November";
			break;
		case 12:
			monthName = "December";
			break;
		default:
			// should never occur after date verification
			exit(-1);

	}

	cout << "This school's starting date is " << monthName << " " << day << ", " << year << endl << endl;

}