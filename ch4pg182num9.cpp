
//
// ch4pg182num9.cpp
//
// Solution to Chapter 4, Page 182, #9
// In Absolute C++, 6th Edition
// 
// Written by Gregory McGuire
//
// https://gregory.red/
// https://twitter.com/gm_mcg
// https://gitlab.com/gmcg
//

// this is going to be one hell of a program
// basically problems 7 and 8 but combined, so lots of code reuse from those two problems

// why are we using if-else statements anyways

#include <iostream> // input/output
//#include <iomanip> // maybe i'll actually use this for once // nope

// constants
// reusing (again) from problem #7
const double POUNDS_IN_1_KG = 2.2046; // the amount of pounds in a kilogram // expected a ;, you better stop that right now VS2017
const int GRAMS_IN_1_KG = 1000; // the amount of grams in a kilogram
const int OUNCES_IN_1_POUND = 16; // the amount of ounces in a pound

using namespace std; // no more std::[whatever], just [whatever]
// it's whatever :shrug:

// functions pt1
void getUserInfo(int metricOrImperial); // get the user's weight measurements
void postResults(int result1, int result2, int metricOrImperial); // post the calculated results to the user's console
int ourSwitchStatement(); // hopefully the last time i reuse this function.
int calcPounds(int kilos, int grams); // given kilograms and grams, calculate pounds.
int calcOunces(int kilos, int grams); // given kilograms and grams, calculate ounces.
int calcKilos(int pounds, int ounces); // given pounds and ounces, calculate kilograms.
int calcGrams(int pounds, int ounces); // given pounds and ounces, calculate grams.

// spooky global variables
int greaterMeasure, // pounds or kilograms
	lesserMeasure; // ounces or grams

int main()
{

	int switchTrack, 
		chooseSystem,
		greaterResult,
		lesserResult;

	// inform the user of what this program actually does.

	cout << endl << "This program will allow you to choose between the Metric (kg/g) or Imperial system (lb/oz)\nand then allow you to input your weight in the system you chose.\n\nYour weight will be converted from Metric -> Imperial and vice versa" << endl << endl;

	for (;;) { // infinite loop
	
		cout << "Weight Conversion\n";
		cout << "----------------------------\n";
		cout << "1. Imperial -> Metric\n";
		cout << "2. Metric -> Imperial\n\n";
		//cout << "3. Quit";

		cout << "Choose an option: ";
		cin >> chooseSystem;
		cout << endl;

		if (chooseSystem == 1) {
		
			getUserInfo(chooseSystem);
			greaterResult = calcKilos(greaterMeasure, lesserMeasure);
			lesserResult = calcGrams(greaterMeasure, lesserMeasure);
			postResults(greaterResult, lesserResult, chooseSystem);

		}
		else if (chooseSystem == 2) {
		
			getUserInfo(chooseSystem);
			greaterResult = calcPounds(greaterMeasure, lesserMeasure);
			lesserResult = calcOunces(greaterMeasure, lesserMeasure);
			postResults(greaterResult, lesserResult, chooseSystem);

		}

		for (; ;) { // switch statement stuff

			switchTrack = ourSwitchStatement();

			if (switchTrack == 1) {

				break; // break out of THIS for loop and continue the outer loop

			}
		}
	
	}

	// the program will end within the for loop, these should never appear.
	// return -1 to signify this as an error
	cout << "This line should never appear!\nWhat did you do?" << endl << endl;
	return -1;

}

// functions pt2

void getUserInfo(int metricOrImperial) {

	if (metricOrImperial == 1) { // i -> m
	
		cout << "Please enter your weight in Pounds and Ounces, separated by a space: ";
		cin >> greaterMeasure >> lesserMeasure;
		cout << endl;

	}
	else if (metricOrImperial == 2) { // m -> i
	
		cout << "Please enter your weight in Kilograms and Grams, separated by a space: ";
		cin >> greaterMeasure >> lesserMeasure;
		cout << endl;

	}

}

void postResults(int result1, int result2, int metricOrImperial) {

	if (metricOrImperial == 1) { // i -> m

		cout << "Your entered weight comes out to " << result1 << " kilograms and " << result2 << " grams.\n\n";

	}
	else if (metricOrImperial == 2) { // m -> i

		cout << "Your entered weight comes out to " << result1 << " pounds and " << result2 << " ounces.\n\n";

	}

}

// copypaste from ch3 projects

int ourSwitchStatement() { // you should know what this is by now

	char willYouContinue; // for the switch statement

	// i made this really generic and now i STILL don't know how i would change it
	cout << "Would you like to enter a new set of data? (Y/N): ";
	cin >> willYouContinue;


	switch (willYouContinue) {

	case 'y':
	case 'Y':
		return 1;
		break;
	case 'n':
	case 'N':
		exit(0); // bye
		break;
	default:
		cout << "Invalid input!" << endl;
		return 0;
		break;
	}

}

// copy our already working functions from problems 7 and 8

int calcPounds(int kilos, int grams) {

	int workingKilos = kilos,
		workingGrams = grams,
		totGrams,
		getPounds;

	totGrams = (workingKilos * GRAMS_IN_1_KG) + workingGrams;
	getPounds = trunc((totGrams / GRAMS_IN_1_KG /* go back to kg for a more accurate result(?) */) * POUNDS_IN_1_KG);

	return getPounds;
	// it works

}

int calcOunces(int kilos, int grams) {

	int workingKilos = kilos,
		workingGrams = grams,
		truncLB,
		getOZ;
	double gramKiloConv,
		getPounds,
		totKilo;

	gramKiloConv = workingGrams / GRAMS_IN_1_KG;
	totKilo = gramKiloConv + workingKilos;

	getPounds = totKilo * POUNDS_IN_1_KG;

	truncLB = trunc(getPounds);

	getOZ = (getPounds - truncLB) * OUNCES_IN_1_POUND;

	return getOZ;

}

int calcKilos(int pounds, int ounces) {

	int calculatedKG;
	double totalLB, workingOZ = ounces, getOZ;

	getOZ = workingOZ / OUNCES_IN_1_POUND;
	totalLB = pounds + getOZ; // user's pounds and ounces = total weight, just to keep things accurate. we'll do this again later
	calculatedKG = totalLB / POUNDS_IN_1_KG; // grams are truncated from the result

	return calculatedKG; // pass the result back to main

}

int calcGrams(int pounds, int ounces) {

	int calculatedGrams, truncKG;
	double totalLB, workingOZ = ounces, getOZ, calculatedKG;

	getOZ = workingOZ / OUNCES_IN_1_POUND;
	totalLB = pounds + getOZ;
	calculatedKG = totalLB / POUNDS_IN_1_KG;
	truncKG = trunc(calculatedKG); // VS2017 likes to suggest the trunc function so I decided to look it up, decided to use it here


	calculatedGrams = (calculatedKG - truncKG) * 1000;

	return calculatedGrams;

}