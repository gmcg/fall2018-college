
//
// ch6pg277num1-single.cpp
// 
// Solution for Chapter 6, Page 277, #1
// Written by Gregory McGuire
//
// twitter/gm_mcg
// gitlab/gmcg
//
// There will be TWO versions of this program.
// One for a single student and another for an entire class.
//

#include <iostream> // i/o
#include <iomanip> // may not be needed

using namespace std;

int getScore(int type); // just so we don't fill main() with the same garbage every time
double calcFinalGrade(int q1, int q2, int mid, int fin, double qweight, double mweight, double fweight); // calculates the final grade, to be shown to the user with a letter grade
void displayFinalGrade(double fingrade); // display the user's final grade

struct classGrades {

	// defaults of zero for reasons/to avoid any issues
	int quiz1Grade = 0; // up to 10 points
	int quiz2Grade = 0; // up to 10 points
	int midtermGrade = 0; // up to 100 points
	int examGrade = 0; // up to 100 points

	double totalGrade; // the student's total grade

};

int main()
{

	classGrades student; // the student's data
	// the multi-student version would put this in as an array

	// grade weights
	double quizGradeWeight = 0.125, // per quiz, both would total to 25%
		midtermGradeWeight = 0.25, // 25%
		examGradeWeight = 0.5; // 50%

	// initial info display for the end-user
	cout << "Grade Calculator" << endl << "-------------------" << endl;
	// display the grade weights, for the user to consider while entering in info
	cout << "Grade Weights." << endl;
	cout << "Quiz: " << quizGradeWeight * 100 << "% (per quiz)" << endl;
	cout << "Midterm Exam: " << midtermGradeWeight * 100 << "%" << endl;
	cout << "Final Exam: " << examGradeWeight * 100 << "%" << endl << endl; // two newlines to make things look a bit nicer

	// put this in a for loop when we work with more than one student
	
	// get info
	student.quiz1Grade = getScore(1);
	student.quiz2Grade = getScore(2);
	student.midtermGrade = getScore(3);
	student.examGrade = getScore(4);
	cout << endl; // space things out

	// calculate the final grade
	student.totalGrade = calcFinalGrade(student.quiz1Grade, student.quiz2Grade, student.midtermGrade, student.examGrade, quizGradeWeight, midtermGradeWeight, examGradeWeight);

	// show the result to the end-user
	displayFinalGrade(student.totalGrade);

	// spacing
	cout << endl;
    return 0;

}


int getScore(int type) {

	int entryType = type, // in case any issues arrive
		toBeReturned; // speaks for itself.


	switch (entryType) {
	
		// could've been worded differently

		case 1:
			// quiz 1
			cout << "What was your Score for your first quiz? (Max: 10): ";
			cin >> toBeReturned;
			return toBeReturned;
			break;
		case 2:
			// quiz 2
			cout << "What was your Score for your second quiz? (Max: 10): ";
			cin >> toBeReturned;
			return toBeReturned;
			break;
		case 3:
			// midterm exam
			cout << "What was your Score for your midterm exam? (Max: 100): ";
			cin >> toBeReturned;
			return toBeReturned;
			break;
		case 4:
			cout << "What was your Score for your final exam? (Max 100): ";
			cin >> toBeReturned;
			return toBeReturned;
			// final exam

			break;
		default:
			cout << "This line should not appear, let me know if it does!" << endl;
			exit(-1);
			break;
	
	}

}

double calcFinalGrade(int q1, int q2, int mid, int fin, double qweight, double mweight, double fweight) {

	// store the passed values in local variables
	int quiz1 = (q1 * 10), // 9 -> 90
		quiz2 = (q2 * 10), // ditto
		midterm = mid,
		finalg = fin;
	double quizW = qweight,
		midW = mweight,
		finW = fweight;

	// final grade stored here
	double finalGrade;

	// calculate...
	// hopefully this works first try
	finalGrade = (quiz1 * quizW) + (quiz2 * quizW) + (midterm * midW) + (finalg * finW);

	return finalGrade;

}

void displayFinalGrade(double fingrade) {

	// store the passed value
	double finalGrade = fingrade;
	// finalGrade / 10 = this
	int finMod; // I was originally going to use modulus but didn't. I kept the name though

	finMod = finalGrade / 10; // should pass a value between 0-10

	// this would probably be overzealous for grading but I don't think it matters
	switch (finMod) {
	
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			// any score below 60 is an F
			cout << fixed << setprecision(2) << showpoint << "Your final grade sits at a " << finalGrade << ", which is an F." << endl;
			cout << "Better luck next time!" << endl;
			break;
		case 6:
			// D
			cout << fixed << setprecision(2) << showpoint << "Your final grade sits at a " << finalGrade << ", which is a D." << endl;
			cout << "Better luck next time!" << endl;
			break;
		case 7:
			// C
			cout << fixed << setprecision(2) << showpoint << "Your final grade sits at a " << finalGrade << ", which is a C." << endl;
			//cout << "You did it!" << endl;
			break;
		case 8:
			// B
			cout << fixed << setprecision(2) << showpoint << "Your final grade sits at a " << finalGrade << ", which is a B." << endl;
			break;
		case 9:
		case 10:
			// A
			cout << fixed << setprecision(2) << showpoint << "Your final grade sits at a " << finalGrade << ", which is an A." << endl;
			break;
		default:
			cout << "It seems like you've managed to gone over 100 on your final grade. That (still) results in an A, good job!" << endl;
			cout << "This could also be the result of a computing or input error, \nI don't think you're supposed to have a grade that is in the negatives..." << endl;
			cout << "Calculated grade: " << fixed << setprecision(2) << showpoint << finalGrade << " (If this is errored, tell the programmer!)" << endl;
			break;

	}

}