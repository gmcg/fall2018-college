
//
// ch2pg96num4.cpp
//
// Solution for Chapter 2, Page 96, Number 4
// In Absolute C++, 6th Edition
//
// Written by: Gregory McGuire
//
// Links:
// https://gregory.red
// https://gitlab.com/gmcg
// https://twitter.com/gm_mcg
//

// Problem in question:
// Write a program that finds and prints all of the prime numbers between 3 and 100.
// A prime number is a number that can only be divided by one and itself(i.e., 3, 5,
// 7, 11, 13, 17, . . .).

// Given that the problem says BETWEEN 3 and 100, it is implied that 3 and 10 IS NOT included in the calculations.
// It didn't explicitly say to include 3 and 100.

// The book suggests a doubly-nested loop
// So a loop within a loop


// begin includes
#include <iostream>
// we're (mainly) working with integers so we will not need iomanip unless we want to make it look pretty

// important
using namespace std; // expected a ;, ; is there, VS2017 what are you doing


// begin main

int main()
{

	// begin variables
	// almost forgot to inlude my initials in each of them, even though they don't look nice now
	int resultCastGM = 0, // the result from the loops is cast to this variable before it is printed onto the console output
		iGM, // for a loop
		cGM, // for another loop
		primeCalcResultGM, // will be either 0 (false) or 1 (true)
		primeWorkingGM; // iGM is cast to this so iGM can be used in equations without accidentally modifying iGM

	// clarify what we're actually doing
	cout << "All the prime numbers between 3 and 100 (excluding 3 and 100) are:";

	// start at 4, end at 99, add one to iGM per pass
	for (iGM = 4; iGM < 100; ++iGM) {

		primeWorkingGM = iGM; // pass the value of iGM to our working variable
		primeCalcResultGM = 1; // set this to true before the 

		// start at 2 just to take 1 outside of the equation
		for (cGM = 2; cGM <= (primeWorkingGM - 1); cGM++) {
		
			// if there is no remainder, set primeCalcResult to 0 (false)
			if (primeWorkingGM % cGM == 0)
			{
				primeCalcResultGM = 0;
			}
			else if (primeWorkingGM % cGM != 0 && primeCalcResultGM != 0) { // if neither of these are true, change primeCalcResult to 1 (true) 
				primeCalcResultGM = 1;
				resultCastGM = primeWorkingGM; // give primeWorking to resultCast so the number can get printed to the console window
			}
			
		}

		if (resultCastGM == 97 && primeCalcResultGM == 1) { // workaround for the last expected number, will change when needed

			// print out a period at the end instead of a comma
			cout << " " << resultCastGM << ".";

		}
		else if (primeCalcResultGM == 1)
			cout << " " << resultCastGM << ",";

	}

	// spacing
	cout << endl << endl;
	return 0;

}