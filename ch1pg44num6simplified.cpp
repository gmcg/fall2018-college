
//
// ch1pg44num6simplified.cpp
// (a more simplified version of ch1pg44num6.cpp)
//
// Solution for Chapter 1, Page 44, Number 6
// In Absolute C++, 6th Edition
//
// Written by: Gregory McGuire
//
// Links:
// https://gregory.red
// https://gitlab.com/gmcg
// https://twitter.com/gm_mcg
//

// For the problem itself, please consult the book.

// My professor has requested that I make some changes (which will not be disclosed in this program's comments), 
// so why don't I make them configurable from the program itself before the user inputs the data?


// includes
#include <iostream> // program output, user input
#include <iomanip> // format adjustment when printing out results
// for the sake of my time, I will not allow the user to load/save settings to a file

// ios::cout -> cout, etc etc
using namespace std; // >expected a ; while it is CLEARLY on this line already

// begin main

int main()
{

	// begin variables
	// use the adjusted values from the assignment as defaults

	const double OVERTIME_RATE_GM = 1.5; // should be self-explanatory, am I even using constants right?
	//const int SENTINEL_GM = -99; // for breaking out of edit mode
	//char switchControllerGM; // allows the user to change the below via a menu (switch statement)
	int healthcareFeeGM = 35, // if the employee has 3 dependents, $35 is taken from the weekly pay
		unionDuesGM = 10, // the union takes $10/week
		regularHoursGM = 40, // per the assignment given to me by the teacher
		hoursWorkedOvertimeGM, // hours worked overtime
		hoursWorkedRegularGM; // hours worked - the overtime hours
	double payPerHourGM = 20.00, // base pay per hour
		socSecurityTaxGM = 0.10, // social security tax, as a percentage
		fedIncomeTaxGM = 0.14, // federal income tax, as a percentage
		stateIncomeTaxGM = 0.05, // state income tax, as a percentage
		totalPayGM = 0, // read the fancy label i used for this variable
		grossPayGM, // ditto
		fedWithheldGM,
		socWithheldGM,
		stateWithheldGM,
		totalWithheldGM;

	// begin user-input related variables, besides switchControllerGM because of poor planning

	int userHoursWorkedGM, // the number of hours the user has worked
		userDependentsGM; // the number of dependents that live in the user's home
						  // todo: the rest if needed

	// begin editor variables

	/*
	int healthcareFeeEditGM,
		unionDuesEditGM,
		regularHoursEditGM;
	double payPerHourEditGM,
		socSecurityTaxEditGM,
		fedIncomeTaxEditGM,
		stateIncomeTaxEditGM;
		*/ // we don't have the editor enabled in this version

	// begin the actual program

	// couldn't come up with a cheesier name
	cout << "Pay Calculator [L I T E] Version 0.0.1b" << endl << endl;

	// show what everything is set to by default
	// simplify what shows up on the user's command window
	/*
	cout << "The current settings go as follows:" << endl;
	cout << "Regular working hours: " << regularHoursGM << endl;
	cout << "Overtime rate (read-only): " << fixed << showpoint << setprecision(2) << OVERTIME_RATE_GM << endl; // debating on making this not read-only sometime in the future
	cout << "The Health Insurance fee (applicable if you have >= 3 dependents): $" << healthcareFeeGM << endl;
	cout << "Your Union Dues: $" << unionDuesGM << endl;
	cout << "Pay per hour: $" << fixed << showpoint << setprecision(2) << payPerHourGM << endl;
	cout << "The Social Security tax (or VAT): " << socSecurityTaxGM * 100 << "%" << endl; // multiply by 100 to get a proper percentage, VAT is used outside of the US
																						   // frankly I don't actually know how VAT works exactly so this could be represented wrong
	cout << "The Federal Income tax: " << fedIncomeTaxGM * 100 << "%" << endl;
	cout << "Your State Income tax: " << stateIncomeTaxGM * 100 << "%" << endl;
	cout << endl;

	// ask if this fits the user's needs
	cout << "Does this look correct to you? (Y/N): ";
	cin >> switchControllerGM;
	*/

	// begin switch statement
	// too complex ;_>;

	/*switch (switchControllerGM) {

	case 'y':
	case 'Y':
		// break out of the switch statement
		break;
	case 'n':
	case 'N':
		// enter edit mode
		cout << endl
			<< "You are now editing the parameters for this program." << endl
			<< "At any point in edit mode, type -99 and press enter to leave edit mode." << endl
			<< "Not following instructions may result in data loss!" << endl << endl;

		cout << "Please enter your work's regular working hours. (Whole number.): ";
		cin >> regularHoursEditGM;
		cout << endl;
		if (regularHoursEditGM == SENTINEL_GM) { cout << endl << endl; break; } // hopefully this will work as intended

		cout << "Please enter a new Health Insurance Fee. (Whole number, round up if needed.): "; // for those who live on free healthcare or similar
		cin >> healthcareFeeEditGM;
		cout << endl;
		if (healthcareFeeEditGM == SENTINEL_GM) { cout << endl << endl; break; }

		cout << "Please enter your Union Dues. (Whole number, round up if needed.): "; // for those who don't fit the default, or are not unionized
		cin >> unionDuesEditGM;
		cout << endl;
		if (unionDuesEditGM == SENTINEL_GM) { cout << endl << endl; break; }

		cout << "Please enter your pay per hour: ";
		cin >> payPerHourEditGM;
		cout << endl;
		if (payPerHourEditGM == SENTINEL_GM) { cout << endl << endl; break; }

		cout << "Please enter a new Social Security tax (or VAT, as a percentage [minus the %]): "; // for those who live outside of the US, i still don't know how VAT works
		cin >> socSecurityTaxEditGM;
		socSecurityTaxEditGM = socSecurityTaxEditGM / 100; // convert to decimal
		cout << endl;
		if (socSecurityTaxEditGM == SENTINEL_GM) { cout << endl << endl; break; }

		cout << "Please enter a new federal income tax. (as a percentage [minus the %]): ";
		cin >> fedIncomeTaxEditGM;
		fedIncomeTaxEditGM = fedIncomeTaxEditGM / 100;
		cout << endl;
		if (fedIncomeTaxEditGM == SENTINEL_GM) { cout << endl << endl; break; }

		cout << "Please enter a new state income tax (as a percentage [minus the %]): ";
		cin >> stateIncomeTaxEditGM;
		stateIncomeTaxEditGM = stateIncomeTaxEditGM / 100;
		cout << endl;
		if (stateIncomeTaxEditGM == SENTINEL_GM) { cout << endl << endl; break; }

		// apply the edits
		regularHoursGM = regularHoursEditGM;
		healthcareFeeGM = healthcareFeeEditGM;
		unionDuesGM = unionDuesEditGM;
		payPerHourGM = payPerHourEditGM;
		socSecurityTaxGM = socSecurityTaxEditGM;
		fedIncomeTaxGM = fedIncomeTaxEditGM;
		stateIncomeTaxGM = stateIncomeTaxEditGM;

		// review changes
		cout << "The new settings go as follows:" << endl;
		cout << "Regular working hours: " << regularHoursGM << endl;
		cout << "Overtime rate (read-only): " << OVERTIME_RATE_GM << endl;
		cout << "The Health Insurance fee (applicable if you have >= 3 dependents): $" << healthcareFeeGM << endl;
		cout << "Your Union Dues: $" << unionDuesGM << endl;
		cout << "Pay per hour: $" << fixed << showpoint << setprecision(2) << payPerHourGM << endl;
		cout << "The Social Security tax (or VAT): " << socSecurityTaxGM * 100 << "%" << endl;
		cout << "The Federal Income tax: " << fedIncomeTaxGM * 100 << "%" << endl;
		cout << "Your State Income tax: " << stateIncomeTaxGM * 100 << "%" << endl;
		cout << endl;

		break;
	default:
		// you didn't enter anything or you entered something invalid, you moron
		cout << "Invalid input!" << endl;
		return -1;

	}*/

	// spacing
	cout << endl << endl;

	// ask the user for    D A T A

	cout << "How many hours did you log? (Enter a whole number.): ";
	cin >> userHoursWorkedGM;
	cout << endl;
	cout << "How many dependents live with you? (Enter a whole number.): ";
	cin >> userDependentsGM;
	cout << endl;
	// everything else is assumed in the settings

	// hourly pay * hours worked regular
	// if hours worked > regular hours, hours worked overtime = hours worked - regular hours
	// gross pay = (hours worked regular * hourly pay) + (hours worked overtime * (hourly pay * overtime rate))
	// taxes should be paid after healthcare (under the implication that the healthcare is provided by the job itself)
	// union dues (to my own understanding) is paid after you recieve the check (therefore, after taxes)
	// total pay =  gross pay - healthcare - the taxes - union dues

	// prep work

	if (userHoursWorkedGM > regularHoursGM) { // compare how many hours the user has worked to the regular hours

											  // calculate how many hours the user worked overtime, 
											  // subtract that from the total hours worked to get the regular work hours
											  // a bit redundant but honestly who cares
		hoursWorkedOvertimeGM = userHoursWorkedGM - regularHoursGM;
		hoursWorkedRegularGM = userHoursWorkedGM - hoursWorkedOvertimeGM;

	}
	else if (userHoursWorkedGM <= regularHoursGM) // again but different
	{
		// honestly what this does should be obvious
		hoursWorkedOvertimeGM = 0;
		hoursWorkedRegularGM = userHoursWorkedGM;
	}

	// calculations :zzz:

	grossPayGM = (hoursWorkedRegularGM * payPerHourGM) + (hoursWorkedOvertimeGM * (payPerHourGM * OVERTIME_RATE_GM)); // i honestly could've checked if the overtime hours were zero before doing this
																													  // below is shown individually at the results
	socWithheldGM = grossPayGM * socSecurityTaxGM;
	fedWithheldGM = grossPayGM * fedIncomeTaxGM;
	stateWithheldGM = grossPayGM * stateIncomeTaxGM;
	totalWithheldGM = socWithheldGM + fedWithheldGM + stateWithheldGM;
	// above is shown individually at the results
	totalPayGM = grossPayGM;

	if (userDependentsGM >= 3) {

		totalPayGM = totalPayGM - healthcareFeeGM; // healthcare before tax

	}

	totalPayGM = totalPayGM - socWithheldGM; // am i deducting taxes correctly
	totalPayGM = totalPayGM - fedWithheldGM;
	totalPayGM = totalPayGM - stateWithheldGM;
	totalPayGM = totalPayGM - unionDuesGM; // union after tax

										   // results

										   //<< fixed << showpoint << setprecision(2) <<
	cout << "Your gross pay (before taxes): $" << fixed << showpoint << setprecision(2) << grossPayGM << endl;
	cout << "The amount withheld due to taxes:" << endl;
	cout << "Social Security: $" << fixed << showpoint << setprecision(2) << socWithheldGM << endl;
	cout << "Federal Income: $" << fixed << showpoint << setprecision(2) << fedWithheldGM << endl;
	cout << "State Income: $" << fixed << showpoint << setprecision(2) << stateWithheldGM << endl << endl;
	cout << "Expenses related to your work:" << endl;

	if (userDependentsGM >= 3) {

		cout << "Healthcare: $" << healthcareFeeGM << endl; // no point as it is a whole value

	}

	cout << "Union dues: $" << unionDuesGM << endl << endl;

	if (userDependentsGM >= 3) {

		cout << "This program deducts taxes after healthcare, and then finishes with your union dues (if any)." << endl << endl;

	}
	else
	{
		cout << "This program deducts taxes and then finishes with your union dues (if any)." << endl << endl;
	}

	cout << "Your total pay (what you go home with): $" << fixed << showpoint << setprecision(2) << totalPayGM;

	// spacing
	cout << endl << endl;
	return 0; // congradulations the program finished without any issues :cheer:

}

// programming soundtrack: Porter Robinson - Worlds (the whole album)
// it is pretty good and you should totally listen to it if you like electronic music
