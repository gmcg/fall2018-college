//
// ch1pg24num7.cpp
//
// Solution to #7 on page 24, chapter 1 of Absolute C++ 6th Edition
//
// Written by: Gregory McGuire
//
// Links:
// https://gitlab.com/gmcg
// https://twitter.com/gm_mcg
//

#include <iostream>
using namespace std;


int main()
{
	int num1GM, num2GM, resultGM;

	cout << " The purpose of this program is to take two numbers, \n divide those two numbers, \n print the result, and also print the remainder." << endl << endl;

	cout << "Please enter a number: "; cin >> num1GM; cout << endl;
	cout << "Please enter a second number: "; cin >> num2GM; cout << endl;

	resultGM = num1GM / num2GM;

	cout << "The quotient of these two numbers is " << resultGM << " with a remainder of " << num1GM % num2GM << ".";

	cout << endl << endl;
    return 0;
}