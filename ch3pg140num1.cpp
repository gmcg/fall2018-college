
//
// ch3pg140num1.cpp
//
// Solution to Chapter 3, Page 140, #1
// In Absolute C++, 6th Edition
//
// Written by Gregory McGuire
// 
// https://gregory.red
// https://twitter.com/gm_mcg
// https://gitlab.com/gmcg
//
// contains some reused code from ch3pg141num5.cpp
//

// Problem in question:
// A liter is 0.264179 gallons. Write a program that will read in the number of liters
// of gasoline consumed by the user’s car and the number of miles traveled by the
// car and will then output the number of miles per gallon the car delivered. Your
// program should allow the user to repeat this calculation as often as the user wishes.
// Define a function to compute the number of miles per gallon. Your program
// should use a globally defined constant for the number of liters per gallon.

// instead of liters -> gallons we're going gallons -> liters
// what if we could just switch between the two huh :V

// includes
#include <iostream> // basic i/o
//#include <iomanip> // output formatting if needed

// important
using namespace std; // expected a ;

// constants
// this would be so much better if i didn't have to add my initials to the end of it
const double A_LITER_IS_GM = 0.264179; // GALLONS

// function prototypes
double getMiPerLit(double miles, double gallons); // run the given values through a calculation, return the result as a double
int ourSwitchStatement(); // reuse some code from ch3pg141num5.cpp
// did we ever go over how header files were formatted? or was I out then? 
// at some point I'd like to keep some of the somewhat useful functions I have in them.

// begin main
int main()
{

	int progTrackGM; // for tracking progress in ourSwitchStatement()
	// use doubles because you can go a fraction of a mile and use a fraction of a ~~liter~~ gallon
	// markdown, if i recall correctly, has the double tildes as strikethrough, too bad you'll never see it here
	double milesTravelledGM, // the amount of miles driven by the user
		   gallonsOfGasConsumedGM, // the amount of gas consumed by the user's car
		   calcMPG; // the result from getMiPerGal()
    
	// print some info for the user
	cout << "This program should not be used for time-sensitive situations!\nPlease consider doing your own math if you're in a real situation!"; // this is for an assignment, you're better off doing the math on your own irl
	cout << endl << "This program will calculate your miles per liter based off of the following:" << endl; // tell the user what info we need and what will come out of this
	cout << " - The miles you have traveled." << endl << " - How much fuel you used during that distance, in gallons.";
	cout << endl << endl;

	for (;;) { // infinite

		// this for loop and another for loop is just another copypaste from my other chapter 3 program
		// but the internals were mostly gutted and replaced with essentially the same thing, but not really

		cout << "How many miles have you traveled?: ";
		cin >> milesTravelledGM; cout << endl;
		cout << "How much gas did you use? (Gallons): ";
		cin >> gallonsOfGasConsumedGM; cout << endl;

		// pass the info to our function so it can do the thing
		calcMPG = getMiPerLit(milesTravelledGM, gallonsOfGasConsumedGM);

		cout << endl;
		cout << "To recap, the amount of miles you traveled is: " << milesTravelledGM << endl;
		cout << "Your mileage is " << calcMPG << " miles per liter." << endl << endl;

		for (;;) { // infinite

			// prompt the user if he/she would like to use a new set of data
			progTrackGM = ourSwitchStatement();

			if (progTrackGM == 1) {

				break; // break out of THIS for loop and continue the outer loop

			}

		}

	}

	// spacing
	cout << endl << endl;
	return 0;

}

// begin functions

// ask the user if he/she wants to enter more data
int ourSwitchStatement() { // a big copypaste from my other chapter 3 assignment

	char willYouContinueGM; // for the switch statement

	cout << "Would you like to enter a new set of data? (Y/N): "; // god i can't really change this up
	cin >> willYouContinueGM;


	switch (willYouContinueGM) {

	case 'y':
	case 'Y':
		return 1;
		break;
	case 'n':
	case 'N':
		exit(0); // bye
		break;
	default:
		cout << "Invalid input!" << endl;
		return 0;
		break;
	}

}

// do the calculation
double getMiPerLit(double miles, double gallons) {

	// we're going from gallons to literes btw
	double calcMilesGM = miles,
		   calcGalGM = gallons,
		   calcLiters,
		   calcResultGM;

	// divide gal by L/gal to get L
	calcLiters = (calcGalGM / A_LITER_IS_GM);

	// miles per liter (mi/L)
	calcResultGM = calcMilesGM / calcLiters;

	return calcResultGM;

}