
//
// ch2pg98num8.cpp
//
// Solution for Chapter 2, Page 98, #8
// In Absolute C++, 6th Edition
//
// Written by Gregory McGuire
//
// https://gregory.red
// https://twitter.com/gm_mcg
// https://gitlab.com/gmcg
//

// Goal: Write a program that finds the temperature, as an integer, that is the same in both Celsius and Fahrenheit.
// F = 9/5(C) + 32

// begin includes
#include <iostream> // input/output

using namespace std;

int main()
{
	
	// begin variables
	int celTempGM = 100, // The book suggests starting from 100 and decrementing from there until we get our result.
	fahTempGM; // conversion from celsius is stored here
	
	// We probably shouldn't print out our progress to the console window.
	
	for(celTempGM = 100 /* probably redundant */; celTempGM >= -100 /* Have a cutoff because this was going on forever. */ && celTempGM <= 100; celTempGM--){
		
			// debug
			//cout << celTempGM << endl;
			
			// swap out 9/5 for 1.8
			// this actually gets the result I wanted
			fahTempGM = 1.8 * celTempGM + 32;
			// debug
			//cout << fahTempGM << "|" << celTempGM << endl;
			
			if(fahTempGM == celTempGM){
				
				// debug
				//cout << fahTempGM << endl;
				break;
				
				}
			
		// expected result: the program stops when fahTempGM == -40
		// but it doesn't :thinking_face:
		// well, it works now
		
		}
	
	// print out the number we eventually got to
	cout << "The point at which Fahrenheit meets Celsius is: " << fahTempGM << endl;
	
	// spacing
	cout << endl << endl;
	return 0;
	
}