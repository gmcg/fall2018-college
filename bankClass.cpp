
//
// bankClass.cpp
//
// written by: gregory mcguire
//
// twitter/gm_mcg
// gitlab/gmcg
//

#include <iostream>
#include <string>
//#include <iomanip> // no true doubles in this

using namespace std;

// classes tend to make my head spin
// then again staying up late to write this isn't helping
// also i would really appreciate it if I had just a few more days to work on this one

class bankAccount { 

	public:
		void setValues(int dollars, int cents, int interest);// actually set the values
		void getInput(string nameof); // get input from user
		void showOutput(string nameof); // show our private data members
	private:
		// leaving all three of these private by choice
		int dollarsPart; // since we're asking for dollars and cents separately, we could just leave these as integers
		int centsPart; // ditto
		int interestRate; // int, will divide by 10 before making any calculations with this

};


int main() // bite sized main()
{

	// objs
	bankAccount checking, savings;
	// strings
	// in hindsight I could've had these as data members in bankAccount but it is a bit too late to worry about that now
	string checkingStr = "Checking",
		savingsStr = "Savings";

	// post part b edit:
	// added some newlines to the member functions to make the output cleaner

	// get input from user
	checking.getInput(checkingStr);
	savings.getInput(savingsStr);

	// show the contents of the checking and savings accounts
	checking.showOutput(checkingStr);
	savings.showOutput(savingsStr);

	// set checking
	checking.setValues(1, 30, 10); // $1.30 with a 10% interest rate

	// show the contents of checking once more
	checking.showOutput(checkingStr);

	// spacing
	cout << endl;
    return 0;
}

void bankAccount::setValues(int dollars, int cents, int interest) {

	// simple
	// no checking because this is controlled

	dollarsPart = dollars;
	centsPart = cents;
	interestRate = interest;

	cout << endl << endl; // two newlines to signify that an edit has occurred

}

void bankAccount::getInput(string nameof) {

	int centsTemp;

	cout << "Editing account " << nameof << endl;
	cout << "How many dollars are in this account? (Whole number.) >> ";
	cin >> dollarsPart; // no verify

	cout << "How many cents are in this account? (Whole number, less than 100) >> ";
	cin >> centsTemp;

	if (centsTemp >= 100) { // if check passes, throw error
	
		cout << "Invalid input! (cents >= 100)";
		exit(-1);

	}

	centsPart = centsTemp;

	cout << "What is the interest rate on this account? (Whole number.) >> ";
	cin >> interestRate;
	cout << endl;

}

void bankAccount::showOutput(string nameof) {

	if (centsPart < 10) {
	
		cout << "The balance of this account is: $" << dollarsPart << ".0" << centsPart << "." << endl;
		cout << "The interest rate is: " << interestRate << "%" << endl << endl;

	}
	else {
	
		cout << "The balance of this account is: $" << dollarsPart << "." << centsPart << "." << endl;
		cout << "The interest rate is: " << interestRate << "%" << endl << endl;
	
	}

}

/*

	assignment

Type parts A and B  in Word. Complete part A and submit it. You must have part A correct before you can go on to part B.  You must have part B correct before you can go on to part C.

Part A: Create a bank account class.  The  private data members include an dollarsPart, centsPart and  interestRate .

The functions in the class include a function to set the private data members (setValues), a function to give value to the private data members (input) and  a function to display the private data members (output).

Part B:Write the definitions for the three functions.

Part C: Copy parts A and B in to Visual Studio.  Create a main function.In the main function create two objects of the BankAccount class named checking and savings.

Call the input function to give values to the private data members as follows:  checking one dollar, 99 cents and 13% interest rate; savings five dollars, 45 cents and 12% interest rate.

Output the value for checking and savings.

Use the setValues function to change the checking values to one dollar, 30 cents and 10% interest rate.

Output the value for checking.

*/