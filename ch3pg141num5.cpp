
//
// ch3pg141num5.cpp
//
// Solution for Chapter 3, Page 141, #5
// In Absolute C++, 6th Edition
//
// Written by Gregory McGuire
//
// https://gregory.red
// https://twitter.com/gm_mcg
// https://gitlab.com/gmcg
//

// the problem in question

/*
5.	 Write a program that asks for the user’s height, weight, and age, and then computes
clothing sizes according to the following formulas.
		•	 Hat size = weight in pounds divided by height in inches and all that multiplied by 2.9.
		•	  Jacket size (chest in inches) = height times weight divided by 288 and then adjusted by adding one-eighth of an inch for each 10 years over age 30. (Note that the adjustment only takes place after a full 10 years. So, there is no adjust- ment for ages 30 through 39, but one-eighth of an inch is added for age 40.)
		•	  Waist in inches = weight divided by 5.7 and then adjusted by adding one-tenth of an inch for each 2 years over age 28. (Note that the adjustment only takes place after a full 2 years. So, there is no adjustment for age 29, but one-tenth of an inch is added for age 30.)
		Use functions for each calculation. Your program should allow the user to repeat this calculation as often as he or she wishes.
*/

#include <iostream> // input/output
#include <iomanip> // output manipulation
//#include <cstdlib> // might need this for exit()?

using namespace std;

// begin function prototypes
// given that functions are basically what the goal of ch3 it would make no sense not to use them
// even though we may only use them once

// it has really been a hot minute since i did these last
// leaving my initials out of these

int ourSwitchStatement();
double getHatSize(double weight, double height); 
double getJacketSize(double weight, double height, int age);
double getWaist(double weight, int age);

// begin main
// one giant infinite loop, have the final prompt either kill the program or go back to the start

int main()
{

	// begin variables
	int userAgeGM, // user's age
	    progTrackGM = 0; // to check if our switch statement completed or not
	double userHeightGM, // user's height (in inches)
		   userWeightGM, // user's weight (in pounds)
		   calcHatSizeGM, // the result of a calculation, the user's predicted hat size
		   calcJacketSizeGM, // the result of a calculation, the user's predicted jacket size
		   calcWaistGM; // the result of a calculation, the user's predicted waist length

	// disclaimer, only needs to be displayed once
	cout << "The information provided by this program may not be accurate! Be sure to double check!" << endl;

	// debug
	//cout << 39 / 10;
	//exit(1);

	for (;;) { // infinite
	
		// request some info about the user of this program
		cout << "Please enter your height (in inches): ";
		cin >> userHeightGM; cout << endl;

		cout << "Please enter your weight (in pounds): ";
		cin >> userWeightGM; cout << endl;

		cout << "Please enter your age: ";
		cin >> userAgeGM; cout << endl;

		// pass off what we got to the functions
		calcHatSizeGM = getHatSize(userWeightGM, userHeightGM);
		calcJacketSizeGM = getJacketSize(userWeightGM, userHeightGM, userAgeGM);
		calcWaistGM = getWaist(userWeightGM, userAgeGM);

		// print out the results to the user
		cout << endl << endl;
		cout << "Your predicted hat size (in inches) is: " << calcHatSizeGM << endl;
		cout << "Your predicted jacket size (in inches, chest width) is: " << calcJacketSizeGM << endl;
		cout << "Your predicted waist size (in inches) is: " << calcWaistGM << endl << endl;

		for (;;) { // infinite
		
			// prompt the user if he/she would like to use a new set of data
			progTrackGM = ourSwitchStatement();

			if (progTrackGM == 1) {

				break; // break out of THIS for loop and continue the outer loop

			}

		}
	
	}

	cout << endl << endl;
	return 0;

}

// begin functions

int ourSwitchStatement() {

	// todo
	char willYouContinueGM; // for the switch statement


	cout << "Would you like to enter a new set of info? (Y/N): ";
	cin >> willYouContinueGM;


	switch (willYouContinueGM) {

	case 'y':
	case 'Y':
		return 1;
		break;
	case 'n':
	case 'N':
		exit(0); // bye
		break;
	default:
		cout << "Invalid input!" << endl << endl;
		return 0;
		break;
	}

}

double getHatSize(double weight, double height) {

	const double HAT_MULTIPLIERGM = 2.9; // as said in the book
	double hatWeightGM = weight, hatHeightGM = height, hatResultGM;

	// easiest function in this program aside from the one that handles the switch statement
	hatResultGM = (hatWeightGM / hatHeightGM) * HAT_MULTIPLIERGM;

	return hatResultGM;

}

double getJacketSize(double weight, double height, int age) {

	// copy paste from the start of this file
	// Jacket size (chest in inches) = height times weight divided by 288 and then adjusted by adding one-eighth of an inch for each 10 years over age 30. 
	// (Note that the adjustment only takes place after a full 10 years. So, there is no adjust- ment for ages 30 through 39, but one-eighth of an inch is added for age 40.)
	// todo
	const int JACKET_DIVIDERGM = 228;
	int eachTenYearsOver30GM, jacketAgeGM = age;
	double jacketWeightGM = weight, jacketHeightGM = height, jacketAdjustGM, jacketResultGM;

	eachTenYearsOver30GM = (jacketAgeGM - 30) / 10;

	if (eachTenYearsOver30GM == 0) { // (39 - 30) / 10 = 0 (each10years being an integer)

		// basically do nothing
		jacketAdjustGM = 0;

	}
	else if (eachTenYearsOver30GM != 0) { // if the user is 40 or older
	
		jacketAdjustGM = eachTenYearsOver30GM * 0.125;

		// debug
		//cout << endl << "jacketAdjustGM == " << jacketAdjustGM;
		//exit(1);

	}

	jacketResultGM = ((jacketHeightGM * jacketWeightGM) / JACKET_DIVIDERGM) + jacketAdjustGM;

	return jacketResultGM;
	

}

double getWaist(double weight, int age) {

	// copypaste from head of file
	// Waist in inches = weight divided by 5.7 and then adjusted by adding one-tenth of an inch for each 2 years over age 28. 
	// (Note that the adjustment only takes place after a full 2 years. So, there is no adjustment for age 29, but one-tenth of an inch is added for age 30.)

	const double WAIST_DIVIDERGM = 5.7;
	int each2YearsAfter28GM, waistAgeGM = age;
	double waistWeightGM = weight, waistAdjustGM, waistResultGM;

	each2YearsAfter28GM = (waistAgeGM - 28) / 2;

	if (each2YearsAfter28GM == 0) { 

		// basically do nothing
		waistAdjustGM = 0;

	}
	else if (each2YearsAfter28GM != 0) { 

		waistAdjustGM = each2YearsAfter28GM * 0.1;

		// debug
		//cout << endl << "waistAdjustGM == " << waistAdjustGM;
		//exit(1);

	}

	waistResultGM = (waistWeightGM / WAIST_DIVIDERGM) + waistAdjustGM;

	return waistResultGM;

}


// programming soundtrack; Orloe - Satellite EP (twice over), Artisan - Bliss (on loop), sanjaux - Trinity (on loop)