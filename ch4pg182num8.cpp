
//
// ch4pg182num7.cpp
//
// Solution for Chapter 4, Page 182, #8
// In Absolute C++, 6th Edition
// 
// Written by Gregory McGuire
//
// https://gregory.red/
// https://twitter.com/gm_mcg
// https://gitlab.com/gmcg
//

/* 
	Write a program like that of the previous exercise that converts from kilograms and
	grams into pounds and ounces. Use functions for the subtasks.
*/

// honestly feels a bit redundant to just take grams and kilos separately when i can just calculate both from the same value

#include <iostream> // input/output
#include <iomanip> // output manipulation

// constants
// reusing from problem #7
// some of these might be unneeded but they're there if we want to use them
const double POUNDS_IN_1_KG = 2.2046; // the amount of pounds in a kilogram
const int GRAMS_IN_1_KG = 1000; // the amount of grams in a kilogram
const int OUNCES_IN_1_POUND = 16; // the amount of ounces in a pound

using namespace std;

// functions pt1
int ourSwitchStatement(); // more code reuse...
void getUserInfo();
int calcPounds(int kilos, int grams);
int calcOunces(int kilos, int grams); // we could probably end up with half of an ounce // decided not to return a double
void postResults(int resLB, int resOZ);

// unsafe global variables
int userKilos, userGrams;

int main()
{
    
	int progTrack,
		calculatedLB,
		calculatedOZ;

	// obligatory info for the user.
	cout << "This program will take your weight (in Kilograms and Grams) and convert\nit into Pounds and Ounces." << endl;

	for (; ;) { // infinite, like the previous problem
	
		getUserInfo();
		calculatedLB = calcPounds(userKilos, userGrams);
		
		// debug
		//cout << endl << calculatedLB << endl;

		calculatedOZ = calcOunces(userKilos, userGrams);
		postResults(calculatedLB, calculatedOZ);

		// debug
		//cout << endl << calculatedOZ;

		for (; ;) { // switch statement stuff

			progTrack = ourSwitchStatement();

			if (progTrack == 1) {

				break; // break out of THIS for loop and continue the outer loop

			}
		}
	
	}

	cout << endl << endl;
	return 0;
}

// functions pt2

int ourSwitchStatement() { // more copypasta

	char willYouContinueGM; // for the switch statement

	// i made this really generic and now i don't know how i would change it
	cout << "Would you like to enter a new set of data? (Y/N): ";
	cin >> willYouContinueGM;


	switch (willYouContinueGM) {

	case 'y':
	case 'Y':
		return 1;
		break;
	case 'n':
	case 'N':
		exit(0); // bye
		break;
	default:
		cout << "Invalid input!" << endl;
		return 0;
		break;
	}

}

void getUserInfo() {

	cout << "Please enter your weight in Kilograms and Grams (Separated by a space.): ";
	cin >> userKilos >> userGrams;

}

int calcPounds(int kilos, int grams) {

	int workingKilos = kilos, 
		workingGrams = grams,
		totGrams,
		getPounds;

	totGrams = (workingKilos * GRAMS_IN_1_KG) + workingGrams;
	getPounds = trunc((totGrams / GRAMS_IN_1_KG /* go back to kg for a more accurate result(?) */) * POUNDS_IN_1_KG);

	return getPounds;
	// it works

}

int calcOunces(int kilos, int grams) {

	int workingKilos = kilos,
		workingGrams = grams,
		truncLB,
		getOZ;
	double gramKiloConv,
		   getPounds,
		   totKilo;

	gramKiloConv = workingGrams / GRAMS_IN_1_KG;
	totKilo = gramKiloConv + workingKilos;

	getPounds = totKilo * POUNDS_IN_1_KG;
	//kiloLBConv = totKilo * POUNDS_IN_1_KG;

	//debug
	//cout << endl << kiloLBConv << endl;
	//cout << endl << getPounds << endl;
	//exit(1);

	truncLB = trunc(getPounds);

	getOZ = (getPounds - truncLB) * OUNCES_IN_1_POUND;

	return getOZ;

}

void postResults(int resLB, int resOZ) {

	cout << "You weigh " << resLB << " Pounds and " << resOZ << " Ounces." << endl << endl;

}